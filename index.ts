import db from './models'
import createApp from './server/app'

async function main() {
    const port = process.env.PORT || '3000'
    await db.sequelize.authenticate()
    const app = createApp()
    app.listen(port, () => {
        console.log(`App is running on port=${port}`)
    })
}

main()
