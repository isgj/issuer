import { Model, Sequelize, DataTypes, Optional } from 'sequelize';

interface AgentAttributes {
    id: number
    name: string
}

interface IAgentCreate extends Optional<AgentAttributes, 'id'> { }

export default (sequelize: Sequelize) => {
    class Agent extends Model<AgentAttributes, IAgentCreate> implements AgentAttributes {
        id!: number
        name!: string

        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models: any) {
            Agent.hasOne(models.Issue, {
                foreignKey: 'agentId'
            })
        }
    };
    Agent.init({
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING(128),
            allowNull: false
        }
    }, {
        sequelize,
        tableName: 'agents',
        modelName: 'Agent',
    });
    return Agent;
};
