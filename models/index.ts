import { Sequelize } from 'sequelize';
import config from '../config/config'
import createAgentModel from './agent'
import createIssueModel from './issue'

const env = process.env.NODE_ENV || 'development';
const dbURL = process.env[config[env]];

if (!dbURL) {
  console.error('Database url is mmissing')
  process.exit(1)
}

const sequelize = new Sequelize(dbURL!, {
  logging: env === 'development' ? console.log : false
})

const models = {
  Agent: createAgentModel(sequelize),
  Issue: createIssueModel(sequelize),
}

Object.values(models).forEach(m => m.associate(models))

const db = {
  sequelize,
  ...models
};

export default db;
