import { Model, DataTypes, Optional, Sequelize } from 'sequelize';

interface IssueAttributes {
  id: number
  content: string
  completed: boolean
  agentId?: number
}

interface IIssueCreate extends Optional<IssueAttributes, 'id' | 'completed'> { }

export default (sequelize: Sequelize) => {
  class Issue extends Model<IssueAttributes, IIssueCreate> implements IssueAttributes {
    id!: number
    content!: string
    completed!: boolean
    agentId?: number

    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models: any) {
      Issue.belongsTo(models.Agent, {
        foreignKey: 'agentId',
        onDelete: 'SET NULL'
      })
    }
  };
  Issue.init({
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    content: {
      type: DataTypes.STRING(256),
      allowNull: false
    },
    completed: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    agentId: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'issues',
    modelName: 'Issue',
  });
  return Issue;
};
