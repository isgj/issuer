### Issuer

After cloning the project instal the dependencies

```bash
npm i
```

It depends only on 1 service a postgres DB. A `docker-compose` file is provided, but you can use another if you already have one running. To bring the DB up

```bash
docker-compose up -d
```

The app needs only the DB URL as an env var `DB_URL`.
If you used the DB from the docker file you can save a `.env` file with

```
DB_URL=postgres://issuer:password@localhost:25432/issuer
```

After that, only the first time run the migrations

```bash
npx sequelize-cli db:migrate
```

then you can start the app

```
npm run dev
```
