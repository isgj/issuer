import { NextFunction, Request, Response } from "express";

export function errorWrapper(fn: (req: Request, res: Response, next?: NextFunction) => any | Promise<any>) {
    return async (req: Request, res: Response, next: NextFunction) => {
        try {
            await fn(req, res, next)
        } catch (error) {
            console.error(error)
            if (error instanceof HTTPError) {
                return res.status(error.code).json(error.data)
            }
            return res.status(500).json({ message: 'Internal error' })
        }
    }
}

export class HTTPError extends Error {
    code: number
    data: any

    constructor(message: string, code: number, data?: any) {
        super(message)
        this.code = code
        this.data = data
    }
}
