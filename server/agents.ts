import { Router } from 'express'
import { body, validationResult } from 'express-validator';
import db from '../models';
import { errorWrapper, HTTPError } from './http_error';


export default function () {
    const agents = Router()
    /**
     * Handy to add new user in the system.
     * No tests or service is used here since this is a prerequsite.
     */
    agents.post(
        '/',
        body('name').isLength({ min: 3, max: 128 }),
        errorWrapper(async (req, res) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                throw new HTTPError('Failed validation', 422, errors.array())
            }
            const agent = await db.Agent.create({ name: req.body.name })
            res.status(201).json(agent)
        })
    )

    return agents
}
