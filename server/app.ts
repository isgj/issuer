import express, { json, } from 'express'
import IssueService from '../services/issue_service'
import createIssueRoutes from './issues'
import createAgentRoutes from './agents'

export default function createApp() {
    const app = express()
    app.use(json())

    const issueService = new IssueService()

    const issueRoutes = createIssueRoutes(issueService)
    const agentRoutes = createAgentRoutes()

    app.use('/issues', issueRoutes)
    app.use('/agents', agentRoutes)
    return app
}
