import { Router } from 'express'
import { body, validationResult } from 'express-validator';
import db from '../models';
import IssueService from '../services/issue_service';
import { errorWrapper, HTTPError } from './http_error';


export default function (issueService: IssueService) {
    const issues = Router()

    issues.post(
        '/',
        body('content').isLength({ min: 3, max: 256 }),
        errorWrapper(async (req, res) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                throw new HTTPError('Failed validation', 422, errors.array())
            }
            const issue = await issueService.createIssue(req.body.content)
            res.status(201).json(issue)
        })
    )

    issues.put('/:id/complete', errorWrapper(async (req, res) => {
        const issueId = Number(req.params.id)
        if (isNaN(issueId)) {
            throw new HTTPError('Bad id', 400, { message: 'Bad id' })
        }

        await issueService.completeIssue(issueId)
        res.json({ message: 'done' })
    }))

    /**
     * Handy to check check the state of the issue
     * not part of the challenge
     */
    issues.get('/', async (req, res) => {
        const completed = req.query.completed !== 'false'
        const issues = await db.Issue.findAll({
            where: {
                completed
            },
            include: { model: db.Agent }
        })

        return res.json(issues)
    })

    return issues
}
