import { config } from 'dotenv'

config()

export default {
  "development": "DB_URL",
  "test": "DB_TEST_URL",
  "production": "DB_URL"
} as Record<string, string>
