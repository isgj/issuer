import { Op } from "sequelize"
import db from "../models"
import { HTTPError } from "../server/http_error"
import IssueService from "./issue_service"

describe('Issue service', () => {
    const issueService = new IssueService()

    beforeEach(async () => {
        await db.Issue.destroy({ where: { id: { [Op.ne]: null } } })
        await db.Agent.destroy({ where: { id: { [Op.ne]: null } } })
    })

    describe('create issue', () => {
        it('creates issue and assigns agent', async () => {
            const agent = await db.Agent.create({
                name: 'name'
            })
            const issue = await issueService.createIssue('content')

            expect(issue.id).toBeGreaterThanOrEqual(1)
            expect(issue.agentId).toBe(agent.id)
        })

        it('does not assign not free agent', async () => {
            const agent = await db.Agent.create({ name: 'name' })
            await db.Issue.create({ content: 'cont', agentId: agent.id })

            const notAssignedIssue = await issueService.createIssue('content')

            const totalIssues = await db.Issue.count()

            expect(totalIssues).toBe(2)
            expect(notAssignedIssue.agentId).toBe(null)
        })

        it('on multiple issue creation assigns free agents only once', async () => {
            const [agent1, agent2] = await Promise.all([
                db.Agent.create({ name: 'agent1' }),
                db.Agent.create({ name: 'agent2' })
            ])

            const issues = await Promise.all(Array(10).fill(0).map(_ => issueService.createIssue('issue')))

            expect(issues.filter(i => !i.agentId).length).toBe(8)
            expect(issues.filter(i => i.agentId === agent1.id).length).toBe(1)
            expect(issues.filter(i => i.agentId === agent2.id).length).toBe(1)
        })
    })

    describe('complete token', () => {
        it('completes issue and makes agent free', async () => {
            const agent = await db.Agent.create({ name: 'agent' })
            const issue = await db.Issue.create({ content: 'cont', agentId: agent.id })
            await db.Issue.create({ content: 'cont', agentId: agent.id, completed: true })

            await issueService.completeIssue(issue.id)

            const agentIssues = await db.Issue.count({ where: { completed: false, agentId: agent.id } })

            expect(agentIssues).toBe(0)
        })

        it('does not assign agent to not completed issue that has already an agent', async () => {
            const agent = await db.Agent.create({ name: 'agent' })
            const another = await db.Agent.create({ name: 'another' })
            await db.Issue.create({ content: 'cont', agentId: another.id, completed: false })
            const issue = await db.Issue.create({ content: 'cont', agentId: agent.id })

            await issueService.completeIssue(issue.id)

            const agentIssues = await db.Issue.count({ where: { completed: false, agentId: agent.id } })

            expect(agentIssues).toBe(0)
        })

        it('complete issue and assign agent only to one not completed issue', async () => {
            const agent = await db.Agent.create({ name: 'agent' })
            const issue = await db.Issue.create({ content: 'cont', agentId: agent.id })
            await Promise.all([
                db.Issue.create({ content: 'cont' }),
                db.Issue.create({ content: 'cont' }),
                db.Issue.create({ content: 'cont' }),
                db.Issue.create({ content: 'cont' })
            ])

            await issueService.completeIssue(issue.id)

            const allIssues = await db.Issue.findAll()

            expect(allIssues.filter(i => i.completed).length).toBe(1)
            expect(allIssues.filter(i => !i.completed).length).toBe(4)
            expect(allIssues.filter(i => i.agentId).length).toBe(2)
            expect(allIssues.filter(i => i.agentId && !i.completed).length).toBe(1)
            expect(allIssues.filter(i => !i.agentId).length).toBe(3)
        })

        it('throws when cannot find issue', () => {
            expect(issueService.completeIssue(0)).rejects.toThrow(HTTPError)
        })

        it('one issue can be completed only once', async () => {
            const agent = await db.Agent.create({ name: 'agent' })
            const issue = await db.Issue.create({ content: 'cont', agentId: agent.id })

            const settle = async <T, E>(p: Promise<T>): Promise<T | Error> => {
                try {
                    return await p
                } catch (e) {
                    return e
                }
            }
            const finished = await Promise.all(Array(10).fill(0).map(_ => settle(issueService.completeIssue(issue.id))))

            expect(finished.filter(f => f instanceof HTTPError).length).toBe(9)
            expect(finished.filter(f => !(f instanceof HTTPError)).length).toBe(1)
        })

        it('free issues are assigned only once', async () => {
            const agents = await Promise.all(Array(10).fill(0).map(_ => db.Agent.create({ name: 'agent' })))
            const issues = await Promise.all(agents.map(a => db.Issue.create({ content: 'c', agentId: a.id })))
            const notAssignedIssues = await Promise.all(agents.map(_ => db.Issue.create({ content: 'c' })))

            const settle = async <T, E>(p: Promise<T>): Promise<T | Error> => {
                try {
                    return await p
                } catch (e) {
                    return e
                }
            }
            // Complete all assigned issues
            await Promise.all(issues.map(i => issueService.completeIssue(i.id)))

            // Reload not assigned issues, now them should be assigned
            await Promise.all(notAssignedIssues.map(i => i.reload()))

            expect(notAssignedIssues.filter(i => i.agentId).length).toBe(10)
            // Each issue should have a different agent assigned
            expect([...new Set(notAssignedIssues.map(i => i.agentId))].length).toBe(10)
        })
    })
})
