import { Op } from "sequelize"
import db from "../models"
import { HTTPError } from "../server/http_error"

const sleep = (t: number) => new Promise(resolve => setTimeout(resolve, t))

export default class IssueService {
    async createIssue(content: string) {
        return await db.sequelize.transaction(async (transaction) => {
            const freeAgent = await db.Agent.findOne({
                where: {
                    // @ts-ignore
                    '$Issue.id$': {
                        [Op.is]: null
                    }
                },
                include: {
                    model: db.Issue,
                    where: { completed: false },
                    required: false
                },
                lock: { level: transaction.LOCK.UPDATE, of: db.Agent },
                skipLocked: true,
                transaction
            })
            const issue = await db.Issue.create({
                content: content,
                agentId: freeAgent?.id
            }, { transaction })
            return issue
        })
    }

    async completeIssue(id: number) {
        return await db.sequelize.transaction(async transaction => {
            const [totalUpdated, issues] = await db.Issue.update(
                { completed: true },
                {
                    where: {
                        id,
                        completed: false,
                        agentId: {
                            [Op.ne]: null
                        }
                    },
                    transaction,
                    returning: ['agentId']
                }
            )

            if (!totalUpdated) {
                throw new HTTPError('No issue found', 404, { message: 'Cannot complete the issue with that id' })
            }

            const freeIssue = await db.Issue.findOne({
                where: { completed: false, agentId: { [Op.is]: null } },
                transaction,
                lock: transaction.LOCK.UPDATE,
                skipLocked: true
            })
            if (!freeIssue) return

            freeIssue.agentId = issues[0].agentId
            await freeIssue.save({ transaction })
        })
    }
}
