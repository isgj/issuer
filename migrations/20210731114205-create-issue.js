'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('issues', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      content: {
        type: Sequelize.STRING(256),
        allowNull: false
      },
      completed: {
        type: Sequelize.BOOLEAN,
        default: false,
        allowNull: false
      },
      agentId: {
        type: Sequelize.INTEGER,
        allowNull: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
    await queryInterface.addConstraint('issues', {
      fields: ['agentId'],
      type: 'foreign key',
      name: 'issues_agents_fk',
      references: {
        table: 'agents',
        field: 'id'
      },
      onDelete: 'set null'
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('issues');
  }
};
